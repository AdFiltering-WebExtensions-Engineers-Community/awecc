# AdFiltering WebExtensions Engineers Community : 
### Introduction
Dedicated for anyone (engineers, managers, techies) in the Ad Filtering and Privacy WebExtensions space dedicated for exchanging experiences and knowledge.


### Connect

- We have slack channel on [Ad Filtering Dev Summit Slack](https://join.slack.com/t/adblockerdeve-olq8702/shared_invite/zt-2ekaget78-W~OY9nPdB4lYI82Dowfv8g) space (search for #browser-extensions-dev-community) where we communicate async. 
- Community meets once a month (2nd Thursday bimonthly) on a 1h call. 
- [Community Calendar](https://calendar.google.com/calendar/u/0?cid=Y18yMDQzZWUzOGQxNTk1YjNlOTY4NjhiYjllNTBmN2IyMTgzYzk3NWVjZTIyMDY2OWRkNzk3MDM4N2I5NGU5MmI1QGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20).
- Feel free to explore the [repo's issues](https://gitlab.com/AdFiltering-WebExtensions-Engineers-Community/awecc/-/issues) where you can discuss a-sync anything related to our community and the technologies we use.

### Code of Conduct 

AdFiltering WebExtensions Engineers Community is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and / or expression, sexual orientation, disability, physical appearance, body size, race, age, religion or lack thereof. We do not tolerate harassment of participants in any form. Sexual language and imagery is not appropriate for any venue, including slack messages, GitLab issues or community calls. Community participants violating these rules may be sanctioned or expelled from the community.

Read the full code of conduct in the [relevant page](/code_of_conduct.md])