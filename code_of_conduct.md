# Community Participation Guidelines
## Purpose

AdFiltering WebExtensions Engineers Community is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and / or expression, sexual orientation, disability, physical appearance, body size, race, age, religion or lack thereof. We do not tolerate harassment of participants in any form. Sexual language and imagery is not appropriate for any venue, including slack messages, GitLab issues or community calls. Community participants violating these rules may be sanctioned or expelled from the community.

## Anti-harassment

### Harassment includes, but is not limited to:

    - Verbal comments that reinforce social structures of domination related to gender, gender identity and / or expression, sexual orientation, disability, physical appearance, body size, race, age, religion
    - Sexual images in public spaces
    - Deliberate intimidation, stalking or following
    - Harassing photography or recording
    - Sustained disruption of talks or other events
    - Inappropriate physical contact
    - Invasion of personal space
    - Unwelcome sexual attention
    - Advocating for, or encouraging, any of the above behaviors

### Enforcement

Community members asked to stop any harassing behavior are expected to comply immediately.

If a community member engages in harassing behavior, other community members retain the right to take any actions to keep the community a welcoming environment for all participants. This includes warning the offender or expulsion from the community.

Organizers may take action to address anything designed to, or with the clear impact of, disrupting the event or making the environment hostile for any participants. We expect participants to follow these rules at all event venues and event-related social activities. We think people should follow these rules outside event activities, too!

### Reporting

If someone makes you or anyone else feel unsafe or unwelcome, please report it as soon as possible. Harassment and other Code of Conduct violations reduce the value of our event for everyone.  People like you make our event a better place. You can make a report either personally or anonymously.
Anti-harassment

You can make a personal report by:
    - Contacting a community maintainer
    - Emailing us: christos AT eyeo DOT com

When taking a personal report, our staff will ensure you are safe and cannot be overheard. They may involve other event staff to ensure your report is managed properly. Once safe, we’ll ask you to tell us about what happened. This can be upsetting, but we’ll handle it as respectfully as possible, and you can bring someone to support you. You won’t be asked to confront anyone and we won’t tell anyone who you are.


In our commitment to a harassment-free and inclusive environment, we strongly believe it’s important to pay attention to harmful language patterns and an intersectional view on discriminations that rarely happen only one-sided. We understand that our list of -isms below is by far not complete, but we believe it’s a good start to raise awareness at our events.

### Ableism

Words like “crazy”, “dumb”, “insane” or “lame” are examples of ableist language, devaluating people who have physical or mental disabilities. Its appearance often stems not from any intentional desire to offend, but from our innate sense of what it means to be normal. These words can be avoided by using more fitting, clearer descriptions of what we want to communicate.

To find out more about ableism and replacement terms, please [read this guide](http://www.autistichoya.com/p/ableist-words-and-terms-to-avoid.html).

### Sexism

Using gendered terms like “dude” or “guys” to address a mixed-gendered group of people contributes to the further exclusion of underrepresented individuals. We strongly advise avoiding gendered pronouns as well as gendered terms. If you’re unsure about people’s pronouns, we highly recommend to ask people directly for their pronouns first and to check if there are already any hints provided by thems (e.g. on badges) instead of assuming their pronouns.

For more information, please familiarize yourself with the [Geek Feminism wiki guide](http://geekfeminism.wikia.com/wiki/Nonsexist_language) and the [Wikipedia Third Person Pronoun](https://en.wikipedia.org/wiki/Third-person_pronoun).
### Racism

Racism is deeply rooted in our society and globally exists among all social classes. It’s connected to [colonialism](https://en.wikipedia.org/wiki/Colonialism), which has a long history of violence, oppression, and domination of one group or individual over another group or individual of a different race, ethnicity, culture, or territory. Addressing individuals or a group of people in a diminutive, derogative or questioning way based on their (assumed) race and ethnic background is therefore racist, disrespectful, and harmful. We do not tolerate any racist behavior, slurs, statements or jokes and will take actions if any reports on the matter reach us.

For more information, please read [Wikipedia Racism](https://en.wikipedia.org/wiki/Racism).

### Attribution

This Code of Conduct is based on [CSSConf / JSConf EU](https://2019.jsconf.eu/code-of-conduct/), The [Geek Feminism wiki](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy), the work of [Valerie Aurora](https://frameshiftconsulting.com/code-of-conduct-training/), and [Conference Code of Conduct](http://confcodeofconduct.com/).

Last update: 11 June 2019 - Version 1.0
